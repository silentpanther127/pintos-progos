//TODO: hier page table entry und page table (hash)
#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <stdio.h>
#include "threads/thread.h"
#include "threads/palloc.h"
#include "lib/kernel/hash.h"
#include "filesys/file.h"

/* Map region identifier */
typedef int mapid_t;

enum suptable_e_src {
    ZEROS,
    FILE
};

struct suptable_e
{
    void *virt_user_add;
    struct hash_elem elem;
    struct file* file;
    off_t ofs;
    uint32_t page_read_bytes;
    uint32_t page_zero_bytes;
    bool writable;
    enum suptable_e_src source;
};

struct mapped_file_e
{
    mapid_t id;
    struct hash_elem elem;
    struct file* file;
    size_t pages;
    void *begin;
};



/*
    Initializes the hashtable SUPTABLE
    returns TRUE on success FALSE otherwise
*/
bool
suptable_init(struct hash *suptable);

/*
    Initializes the hashtable SUPTABLE
    returns TRUE on success FALSE otherwise
*/
bool
mapped_files_init(struct hash *mapped_files);

/*
    destroys the hashtable SUPTABLE
*/
void
suptable_destroy(struct hash *suptable);

/*
    destroys the hashtable SUPTABLE
*/
void
mapped_files_destroy(struct hash *mapped_files);

/*
    Inserts a new entry into the suptable of the current thread
    returns TRUE on success FALSE otherwise
*/
bool
suptable_insert(struct file *file, off_t ofs, uint8_t *upage,
                     uint32_t page_read_bytes, uint32_t page_zero_bytes, 
                     bool writeable);

bool 
suptable_insert_mapped(struct file *file, off_t ofs, uint8_t *upage,
                            uint32_t page_read_bytes, uint32_t page_zero_bytes);

bool
suptable_append_zero(uint8_t *upage);

/*
    gets the suptable entry VIRT_USER_ADD from SUPTABLE and returns it
    returns NULL when failed;
*/
struct suptable_e *
suptable_get(struct hash *suptable, void *virt_user_add);

/*
    loads PAGE from the data given in the entry
*/
bool 
suptable_e_load(struct suptable_e *page);

void
suptable_e_print(struct suptable_e *page);

bool
mapped_file_insert(struct hash* mapped_files, struct file* file, size_t pages, uint32_t read_bytes, void *location, mapid_t id);

bool
mapped_file_remove(struct hash* mapped_files, struct hash* suptable, uint32_t *pagedir, mapid_t id);

#endif