//TODO: hier funktionen zur page table
#include "vm/page.h"
#include "userprog/process.h"
#include "threads/malloc.h"
#include "lib/string.h"
#include "userprog/pagedir.h"
#include "threads/vaddr.h"

static unsigned
page_hash(const struct hash_elem *elem, void *aux UNUSED);

static bool
page_less(const struct hash_elem *a, const struct hash_elem *b, void *aux UNUSED);

static void
page_free(struct hash_elem* elem, void* aux UNUSED);

static unsigned
mapped_file_hash(const struct hash_elem *elem, void *aux UNUSED);

static bool
mapped_file_less(const struct hash_elem *a, const struct hash_elem *b, void *aux UNUSED);

static void
mapped_file_free(struct hash_elem* elem, void* aux UNUSED);

static struct suptable_e *
suptable_delete(struct hash *suptable, void *virt_user_add);

bool suptable_init(struct hash *suptable)
{
    return hash_init(suptable, page_hash, page_less, NULL);
}

void suptable_destroy(struct hash *suptable)
{
    hash_destroy(suptable, &page_free);
}

bool mapped_files_init(struct hash *mapped_files)
{
    return hash_init(mapped_files, mapped_file_hash, mapped_file_less, NULL);
}

void mapped_files_destroy(struct hash *mapped_files)
{

    hash_destroy(mapped_files, &mapped_file_free);
}

static unsigned
page_hash(const struct hash_elem *elem, void *aux UNUSED)
{
    const struct suptable_e *p = hash_entry(elem, struct suptable_e, elem);
    return hash_bytes(&p->virt_user_add, sizeof p->virt_user_add);
}

static bool
page_less(const struct hash_elem *a, const struct hash_elem *b, void *aux UNUSED)
{
    const struct suptable_e *page_a = hash_entry(a, struct suptable_e, elem);
    const struct suptable_e *page_b = hash_entry(b, struct suptable_e, elem);

    return page_a->virt_user_add < page_b->virt_user_add;
}

static void
page_free(struct hash_elem* elem, void* aux UNUSED)
{
    struct suptable_e *page = hash_entry(elem, struct suptable_e, elem);
    free(page);
}

static void
mapped_file_free(struct hash_elem* elem, void* aux UNUSED)
{
    struct mapped_file_e *mapped_file = hash_entry(elem, struct mapped_file_e, elem);

    size_t i;
    struct suptable_e page;
    page.virt_user_add = mapped_file->begin;

    for (i = 0; i < mapped_file->pages; i++)
    {
        struct suptable_e *found_page = suptable_delete(&thread_current()->suptable, page.virt_user_add + i * PGSIZE);
        if(NULL != found_page && pagedir_is_dirty(thread_current()->pagedir, found_page->virt_user_add)){
            process_lock_filesys();
            file_write_at(mapped_file->file,found_page->virt_user_add,found_page->page_read_bytes,found_page->ofs);
            process_unlock_filesys();
            free(found_page);
        }
    }
    process_lock_filesys();
    file_close(mapped_file->file);
    process_unlock_filesys();
    free(mapped_file);
}

static unsigned
mapped_file_hash(const struct hash_elem *elem, void *aux UNUSED)
{
    struct mapped_file_e *mapped_file = hash_entry(elem, struct mapped_file_e, elem);
    return hash_int(mapped_file->id);
}

static bool
mapped_file_less(const struct hash_elem *a, const struct hash_elem *b, void *aux UNUSED)
{
    struct mapped_file_e *mapped_file_a = hash_entry(a, struct mapped_file_e, elem);
    struct mapped_file_e *mapped_file_b = hash_entry(b, struct mapped_file_e, elem);

    return mapped_file_a->id < mapped_file_b->id;
}



bool suptable_insert(struct file *file, off_t ofs, uint8_t *upage,
                     uint32_t page_read_bytes, uint32_t page_zero_bytes,
                     bool writable)
{
    struct suptable_e *page;
    struct thread *cur = thread_current();

    page = malloc(sizeof *page);
    ASSERT(page != NULL);

    page->virt_user_add = upage;
    page->file = file;
    page->ofs = ofs;
    page->page_read_bytes = page_read_bytes;
    page->page_zero_bytes = page_zero_bytes;
    page->source = FILE;
    page->writable = writable;

    bool success = (NULL == hash_insert(&cur->suptable, &page->elem));
    return success;
}

bool suptable_insert_mapped(struct file *file, off_t ofs, uint8_t *upage,
                            uint32_t page_read_bytes, uint32_t page_zero_bytes)
{
    struct suptable_e *page;
    struct thread *cur = thread_current();

    page = malloc(sizeof *page);
    ASSERT(page != NULL);

    page->virt_user_add = upage;
    page->file = file;
    page->ofs = ofs;
    page->page_read_bytes = page_read_bytes;
    page->page_zero_bytes = page_zero_bytes;
    page->source = FILE;
    page->writable = true;

    bool success = (NULL == hash_insert(&cur->suptable, &page->elem));
    return success;
}

bool
suptable_append_zero(uint8_t *upage){
    struct suptable_e *page;
    struct thread *cur = thread_current();

    page = malloc(sizeof *page);
    ASSERT(page != NULL);

    page->virt_user_add = upage;
    page->source = ZEROS;
    page->writable= true;

    bool success = (NULL == hash_insert(&cur->suptable, &page->elem));
    return success;
}

struct suptable_e *
suptable_get(struct hash *suptable, void *virt_user_add)
{
    struct suptable_e page;
    struct hash_elem *page_elem;

    page.virt_user_add = virt_user_add;
    page_elem = hash_find(suptable, &page.elem);

    if (page_elem == NULL)
        return NULL;
    else
        return hash_entry(page_elem, struct suptable_e, elem);
}

static struct suptable_e *
suptable_delete(struct hash *suptable, void *virt_user_add)
{
    struct suptable_e page;
    struct hash_elem *page_elem;

    page.virt_user_add = virt_user_add;
    page_elem = hash_delete(suptable, &page.elem);

    if (page_elem == NULL)
        return NULL;
    else
        return hash_entry(page_elem, struct suptable_e, elem);
}

bool suptable_e_load(struct suptable_e *page)
{
    uint8_t *kpage;
    switch (page->source)
    {
    case FILE:
        file_seek(page->file, page->ofs);

        kpage = palloc_get_page(PAL_USER);
        if (kpage == NULL)
            return false;

        /* Load this page. */
        if (file_read(page->file, kpage, page->page_read_bytes) != (int)page->page_read_bytes)
        {
            palloc_free_page(kpage);
            return false;
        }
        memset(kpage + page->page_read_bytes, 0, page->page_zero_bytes);
        break;
    case ZEROS:
        kpage = palloc_get_page(PAL_USER | PAL_ZERO);
        if (kpage == NULL)
            return false;
        break;
    default:
        PANIC ("reached unreachable statement. No page source found\n");
        break;
    }

    /* Add the page to the process's address space. */
    if (!install_page(page->virt_user_add, kpage, page->writable))
    {
        palloc_free_page(kpage);
        return false;
    }
    return true;
}

void
suptable_e_print(struct suptable_e *page){
    printf("page: [%p; rb:%d; zb:%d; w?:%s]\n", page->virt_user_add, page->page_read_bytes, page->page_zero_bytes, page->writable ? "w" : "nw");
}

bool
mapped_file_insert(struct hash* mapped_files, struct file* file, size_t pages, uint32_t read_bytes, void *location, mapid_t id)
{
    struct mapped_file_e *mapped_file;
    size_t i;

    mapped_file = malloc(sizeof *mapped_file);
    ASSERT(mapped_file != NULL);

    mapped_file->id = id;
    mapped_file->file = file;
    mapped_file->pages = pages;
    mapped_file->begin = location;

    uint32_t rem_read_bytes = read_bytes;
    bool success;
    for (i = 0; i < pages; i++)
    {
        if (rem_read_bytes < PGSIZE){
            success = suptable_insert_mapped(file,i * PGSIZE,location+(i * PGSIZE),rem_read_bytes,PGSIZE-rem_read_bytes);
        } else {
            success = suptable_insert_mapped(file,i * PGSIZE,location+(i * PGSIZE),PGSIZE,0);
        }
        if(!success)
            return false;
        rem_read_bytes -= PGSIZE;
    }

    return (NULL == hash_insert(mapped_files, &mapped_file->elem));
}

bool
mapped_file_remove(struct hash* mapped_files, struct hash* suptable, uint32_t *pagedir, mapid_t id)
{
    struct mapped_file_e mapped_file;
    mapped_file.id = id;

    struct hash_elem *found_elem;
    found_elem = hash_delete(mapped_files, &mapped_file.elem);
    if(NULL == found_elem){
        return false;
    }

    struct mapped_file_e *found_file = hash_entry(found_elem, struct mapped_file_e, elem);

    size_t i;
    struct suptable_e page;
    page.virt_user_add = found_file->begin;

    for (i = 0; i < found_file->pages; i++)
    {
        struct suptable_e *found_page = suptable_delete(suptable, page.virt_user_add + i * PGSIZE);
        if(NULL != found_page && pagedir_is_dirty(pagedir, found_page->virt_user_add)){
            process_lock_filesys();
            file_write_at(found_file->file,found_page->virt_user_add,found_page->page_read_bytes,found_page->ofs);
            process_unlock_filesys();
            free(found_page);
        }
    }
    process_lock_filesys();
    file_close(found_file->file);
    process_unlock_filesys();
    free(found_file);
    return true;
}